#!/usr/bin/env bash

source $(dirname $0)/common-test.sh
source $(dirname $0)/api.sh

function api_pr_test() {
    test_forgejo_setup
    (
	cd $TMP_DIR/origin
	git checkout -b other
	echo OTHER > README.txt
	git commit -m 'OTHER' -a
	git push origin other
    )
    api_check_token    
    ! api_pr_exists main other
    api_pr_create main other "the title" "the body"

    api_pr_exists main other
    api_pr_create main other "the title" "the body"

    local id=$(api_pr_id main other)
    api_pr_close $id
    test "$(api_pr_get $id | jq --raw-output .state)" = "closed"
    api_pr_close $id    
}
